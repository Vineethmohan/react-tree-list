import React from 'react';
import {render} from 'react-dom';
import TreeList from './js/TreeList';
import StarRatingComponent from 'react-star-rating-component';


import {DATA} from './sample-data/tree-data-nested';

const COLUMNS = [
   {
  title: 'Region',
  field: 'firstName',
  type: 'string',
  expand: true,
}, {
  title: 'Location Count',
  field: 'lastName',
  type: 'string'
}, {
  title: 'Rating',
  field: 'employeeId',
  type: 'number',
  class: 'red',
  formatter: function(value) {
    if (value) {
      return 'EMPID' + value;
    }
  }
}, {
  title: 'Joined on',
  field: 'joinedOn',
  type: 'date',
  format: 'dd/mm/yyyy',
  formatter : function(){
    return <Formatter />;
  }
}];

const OPTIONS = {
  height: 350,
  minimumColWidth: 100,
  expandAll: true
};

class App extends React.Component {
  render () {
    return (
      <TreeList
        data={DATA}
        columns={COLUMNS}
        options={OPTIONS}
        id={'id'}
        parentId={'parentId'}></TreeList>
    );
  }
}

class Formatter extends React.Component {
  render () {
    return (
      <div>
        <StarRatingComponent
            name="rate1"
            starCount={5}
            value={2}
        />
      </div>
    );
  }
}


render(<App/>, document.getElementById('app'));
